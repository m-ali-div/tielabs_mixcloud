<?php
/*
Plugin Name: Tielabs mixcloud
Plugin URI: https://tielabs.com//
Description: mixcloud info
Version: 0.1
Author: Mohamed Ali
Author URI: https://tielabs.com//
License: GPLv2
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
   
//plugin version
if ( ! defined( 'TIELABS_MIXCLOUD_VERSION' ) ) {
	define( 'TIELABS_MIXCLOUD_VERSION', 1.0 );
}
//plugin assets url
define( 'TIELABS_ASSETS', plugin_dir_url( __FILE__ ) . 'assets/' );
//plugin path
define( 'TIELABS_PL_PATH', plugin_dir_path( __FILE__ )  );
//plugin inc
define( 'TIELABS_PL_PATH_INC', plugin_dir_path( __FILE__ ) . 'inc/' );
//plugin DIR
define('TIELABS_PL_DIR', __FILE__);
/**
 * Add Plugin Styles & Scripts
 */
add_action( 'wp_enqueue_scripts', 'TIELABS_assets' );
function TIELABS_assets() {
	wp_enqueue_style( 'TIELABS_MIXCLOUD_style', TIELABS_ASSETS . 'css/TIELABS_MIXCLOUD_style.css', false, TIELABS_MIXCLOUD_VERSION  );
	// wp_enqueue_script( 'TIELABS_MIXCLOUD_js', TIELABS_ASSETS . 'js/TIELABS_new_main.js', array( 'jquery' ), TIELABS_MIXCLOUD_VERSION , true );
}


include TIELABS_PL_PATH_INC . 'functions.php'; //Load functions
include TIELABS_PL_PATH_INC . 'shortcode.php'; //Load shortcode
include TIELABS_PL_PATH_INC . 'Widget.php'; //Load Widget
