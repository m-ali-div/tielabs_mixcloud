<?php
class TIELABS_MIXCLOUD_Widget extends WP_Widget {

    function __construct() {

        parent::__construct(
            'TIELABS-MIXCLOUD-Widget',
            'MIXCLOUD BY TIELABS'
        );

        add_action( 'widgets_init', function() {
            register_widget( 'TIELABS_MIXCLOUD_Widget' );
        });

    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['name'] ) ) {
            echo tielabs_mixcloud_template ($instance['name']);
        }
        echo $args['after_widget'];

    }

    public function form( $instance ) {

        $name = ! empty( $instance['name'] ) ? $instance['name'] : esc_html__( '', 'tielabs_mixcloud' );
        ?>
        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'name' ) ); ?>"><?php echo esc_html__( 'name:', 'tielabs_mixcloud' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'name' ) ); ?>" type="text" value="<?php echo esc_attr( $name ); ?>">
        </p>
        <?php

    }

    public function update( $new_instance, $old_instance ) {

        $instance = array();
        $instance['name'] = ( !empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
        return $instance;
    }

}
$TIELABS_MIXCLOUD_Widget = new TIELABS_MIXCLOUD_Widget();
?>
