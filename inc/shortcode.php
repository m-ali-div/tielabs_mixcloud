<?php
function TIELABS_MIXCLOUD_shortcode( $atts, $content = null ) {
	$data = shortcode_atts(['name' => 'lowlight'], $atts );
   return tielabs_mixcloud_template ($data['name']) ;
}

add_shortcode( 'TIELABS_MIXCLOUD', 'TIELABS_MIXCLOUD_shortcode' );
// add_shortcode ('TIELABS_MIXCLOUD', '__return_false');
