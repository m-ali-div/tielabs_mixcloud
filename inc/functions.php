<?php
function tielabs_mixcloud_data ($username) {
  $cache_key = 'prefix_tielabs_mixcloud_'. $username;
  $mixcloud_data = wp_cache_get( $cache_key );
  if ($mixcloud_data != true) {
    $url= 'https://api.mixcloud.com/'.$username;

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $mixcloud_data = curl_exec($curl);
    $mixcloud_data = json_decode($mixcloud_data) ;
    if (!$mixcloud_data->error) {
      wp_cache_set( $cache_key, $mixcloud_data ,'' , 12 * (60*60) );
    }else {
      $mixcloud_data = 'error' ;
    }
  }
  return $mixcloud_data;
}

function tielabs_mixcloud_template ($username) {
  $data = tielabs_mixcloud_data ($username) ;
  if($data != 'error'):
?>
<div class="tielabs_mixcloud">
  <div class="tielabs_mixcloud_head">
    <div class="mixcloud_head_image">
      <img src="<?php echo $data->pictures->medium ; ?>" alt="<?php echo $data->name ; ?>">
    </div>
    <div class="mixcloud_head_content">
      <h3><?php echo $data->name ; ?></h3>
      <h4><?php echo $data->city .' ' .$data->country  ; ?></h4>
      <h5><?php echo $data->follower_count ; ?><?php echo esc_html__( 'Followers', 'tielabs_mixcloud' ); ?></h5>
    </div>
  </div>
  <main class="tielabs_mixcloud_main">
    <p><?php echo $data->biog ; ?></p>
  </main>
  <div class="tielabs_mixcloud_footer">
    <a href="<?php echo $data->url ; ?>" target="_blank" class="Follow_link">  <?php echo esc_html__( 'Follow', 'tielabs_mixcloud' ); ?> </a>
  </div>
</div>
<!-- tielabs_mixcloud -->

<?php
endif;

}
